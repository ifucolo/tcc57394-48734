#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netdb.h>
#include <string.h>
#include <math.h>
#include <netinet/in.h>
#include <unistd.h>
#include <crypt.h>
#include "consulta.h"

#define tam 100
#define niv 6
#define ntask 3

int ack[99],mcadast[tam][niv],pos;
int TREC[tam], TREA[tam], TCOR[tam], CAT[tam];

struct prod
{
	int ack;
	int ident;
	int task;
	int ma[2][2];
	int mb[2][2];
	int mr[2][2];		
};
struct prod mid;

struct prod matrix[100];

struct hashn
{
	char hf[30];
	char hi[4];
};

struct hashn ver;
struct hashn htask[ntask];
struct hashn htaskf[ntask];
struct hashn mconf;

struct log
{
	int log[10];
 
};
struct log lg;
struct log l[tam];

atualiza(int ID,int tipo)
{

	// tipo == 0 eh trec, tipo == 1 eh trea, tipo == 2 eh tcor.
	// categoria: diamante = 0, ouro = 1, prata = 2, bronze = 3, sem-categoria = 4;
	
		int i;
	
		if(tipo == 0){
		TREC[ID]++;
		printf("ID: %d \nTarefas recebidas: %d \n", ID, TREC[ID]);
		}
	
		if(tipo == 1){
		TREA[ID]++;
		printf("Tarefas realizadas: %d \n", TREA[ID]);
		}
	
		if(tipo == 2){
		TCOR[ID]++;
		CAT[ID] = consulta(TREC[ID], TREA[ID], TCOR[ID]);
		printf("Tarefas corretas: %d \n", TCOR[ID]);
		//printf("\nCategoria: %d\n", CAT[ID]);
		}
		if(tipo == 3)
		{
			CAT[ID] = consulta(TREC[ID], TREA[ID], TCOR[ID]);

		}

}

void vercadastro(int t,int a,int b,int c)
{
	
	int i,j;
	
	if(t == 1)// verifica se o cliente existe , se ele nao existe é efetuado o cadastro do mesmo, se não continua normalmente 
	{
		if(mcadast[0][0] == 0)
		{
			mcadast[0][0] = mid.ident;
			pos = 0;
			
			
		}
		else
		{
			for(i = 0;i < tam;i++)
			{
				if(mcadast[i][0] == mid.ident)
				{
					
					pos = i;
					break;
				}
				else if(mcadast[i][0] == 0)
				{	
					mcadast[i][0] = mid.ident;
							
					pos = i;							
					break;
				}

			}	



		}
		
		
	}
	else if(t == 2)//atualiza tarefa e confiaça do client
	{
		
			
		
	}
	

}


void iniciaVerifica()
{
	
		
		int  portno,n,i,j;
		long sockfd;
		struct sockaddr_in serv_addr;
		struct hostent *server;	
		portno = 40000;
		
		sockfd = socket(AF_INET, SOCK_STREAM, 0);
		if (sockfd < 0) 
		{
			perror("ERRO ao abrir o socket");
			exit(1);
		}
	
		server = gethostbyname("localhost");
		if (server == NULL) 
		{
			fprintf(stderr,"2ERRO, Sem servidor\n");
			exit(0);
		}

		bzero((char *) &serv_addr, sizeof(serv_addr));
		serv_addr.sin_family = AF_INET;
		bcopy((char *)server->h_addr, (char *)&serv_addr.sin_addr.s_addr, server->h_length);
		serv_addr.sin_port = htons(portno);
	
	
		if (connect(sockfd,(struct sockaddr *)&serv_addr,sizeof(serv_addr)) < 0) 
		{
			perror("ERRO ao se conectar");
			exit(1);
		}
		
		mid.task = 1;
		n = send(sockfd, &mid, sizeof(struct prod), 0);//envia a tarefa para o verificados
		
		n = recv(sockfd, &ver, sizeof(struct hashn), 0);//recebe o resultado
		strcpy(mconf.hf,ver.hf);
		
		//printf("\nrecebi\n");					
		
		for(i = 0;i < 3;i++)
		{		
			ack[i] = 0;
		}

}
void salvalog(int task,int id)
{
	int i,j;
	for(j = 0;j < tam;j++)
	{
		if(id == j)
		break;
	}
	for(i = 0;i < 10;i++)
	{
		if(l[j].log[i] != 0 || l[j].log[i] != task)
		{
			l[j].log[i] = task;
			//printf(" log id = %d task = %d ",j,l[j].log[i]);
			break;
		}	

	}
	


}
int metrica(int cli[ntask],int cat[ntask])
{

//categoria: diamante = 0, ouro = 1, prata = 2, bronze = 3, sem-categoria = 4;

	float diamante = 0, ouro = 0, prata = 0, bronze = 0;
	int i;
	for(i = 0;i < ntask;i++)
	{
		if(cat[i] == 0)
			diamante = diamante + 1.0;
		if(cat[i] == 1)
			ouro = ouro + 1.0;
		if(cat[i] == 2)
			prata = prata + 1.0;
		if(cat[i] == 3)
			bronze = bronze + 1.0;		

	}	
	

	if((diamante >= (30.0*ntask/100.0)) && ((ouro >= (30.0*ntask/100.0)) || (prata >= (30.0*ntask/100.0)))){

		printf("diamante: %f porcentagem minima: %f\n", diamante, 30.0*ntask/100.0);
		printf("Ok, não precisa verificar!\n");
		return 1;
	}
	if((ouro >= (60.0*ntask/100.0)) && ((prata >= (30.0*ntask/100.0)) || (bronze >= (30.0*ntask/100.0)))){

		printf("Ok, não precisa verificar!\n");
		return 2;
	}
	else{

		printf("Precisa verificar!\n");
		return 0;
	}





}
void hash(int vec[ntask])
{

	char salt[] = "$1$";
	int a,b,c,i,cat[ntask],vrp[ntask],j,diamante,ouro,vresp[ntask - 1];
	printf("\nHASH\n");
	c = ntask;
	// abaixo transforma a matriz 2x2 em uma string	
	
	for(i = 0;i < ntask;i++)//calcula as hashs do cliente
	{
		sprintf(htask[i].hi,"%d%d%d%d",matrix[vec[i]].mr[0][0],matrix[vec[i]].mr[0][1],matrix[vec[i]].mr[1][0],matrix[vec[i]].mr[1][1]);		
		strcpy(htaskf[i].hf,crypt(htask[i].hi,salt));
		
		printf("\n hash = %s do cliente = %d\n",htaskf[i].hf,vec[i]);
	}
	
	for(i = 0; i < ntask;i++)
	{	
		atualiza(vec[i],3);		
		//printf(" categoria %d do %d\n",CAT[i],vec[i]);
		vrp[i] = CAT[i];
		if(vrp[i] == 0)
			diamante = vec[i];
		if(vrp[i] == 1)
			ouro = vec[i];	
	}
	
	a = metrica(vrp,vec);
	if(a == 1)
	{
		
		for(i = 0;i < ntask;i++)
		{
			if(diamante != vec[i])
			{
				if(strcmp(htaskf[diamante].hf,htaskf[i].hf) != 0)
				{
					c--;
					vresp[i] = vec[i];

				}
				else
				{			
					
					vresp[i] = 0;
					atualiza(vec[i],2);	
				}
			}
			if(c == ntask)
				atualiza(vec[diamante],2);		
		}

	}
	if(a == 2)
	{
		
		for(i = 0;i < ntask - 1;i++)
		{
			if(ouro != vec[i])
			{
				if(strcmp(htaskf[ouro].hf,htaskf[i].hf) != 0)
				{
					c--;
					vresp[i] = vec[i];

				}
				else
				{			
					vresp[i] = 0;
					atualiza(vec[i],2);				
				}
			}
			if(c == ntask)
				atualiza(vec[ouro],2);		
		}

	}
	else if(a == 0)
	{
		iniciaVerifica();	

		for(i = 0;i < ntask;i++)
		{
			if(strcmp(mconf.hf,htaskf[i].hf) != 0)
			{
				printf("\n o %d é hacker",vec[i]);
				salvalog(mid.task,vec[i]);

			}
			else
			{
				printf("\n%d é confiavel e atualiza",vec[i]);
				atualiza(vec[i],2);
				salvalog(mid.task,vec[i]);
				
				
			}		
		}
	}
	//vercadastro(2,a,b,c);// atualiza o cadastr*/
}
void *func (void *sock)
{
	
	long socket;
	int n,i,j = 0,vec[ntask-1],pp[2];
	socket=(long )sock;
	
	
	n = recv(socket,&mid,sizeof(struct prod), 0);
	vercadastro(1,0,0,0);// verifica se o cliente é cadastrado se sim envia a tarefa se nao cadastra e depois envia tarefa
		

	for(i = 0;i < 2;i++)
	{
		for(j = 0;j < 2;j++)
		{
			mid.ma[i][j] = 2;
			mid.mb[i][j] = 2;
			mid.mr[i][j] = 0;
		}
	}
	mid.task = 1;
	mid.ack = 0;
	
	n = send(socket,&mid,sizeof(struct prod), 0);//envia a tarefa e as matrizes 
	atualiza(mid.ident,0);
	
	n = recv(socket,&mid,sizeof(struct prod), 0);//recebe o resultado
	atualiza(mid.ident,1);
	
	//ack = ack + mid.ack;
	
	for(i = 0;i < 2;i++)
	{
		for(j = 0;j < 2;j++)
		{
			matrix[mid.ident].mr[i][j] = mid.mr[i][j];//armazena a matriz resultado processada pelo cliente
		}
	}
	//printf(" pos  = %d\n",pos);
	mcadast[pos][4] = mid.ack;//ack
	mcadast[pos][3] = mid.task;
	
	printf(" \né o cliente %d\n ",mid.ident);
		
	j = 0;
	if(mcadast[pos][4] == 1)
	{	
		for(i = 0;i < tam;i++)
		{
			if(mcadast[i][0] != mcadast[pos][0] && mcadast[i][0] != 0)
			{	
				//printf("pos=%d  mi=%d i=%d\n",mcadast[pos][0],mcadast[i][0],i);		
				
					
					if(mcadast[i][3] == mcadast[pos][3])
					{
						
						if(mcadast[i][4] == 1)
						{
							//printf(" pppj = %d ntask = %d\n",j,ntask);	
							vec[j] = mcadast[i][0];//salva os outros 2 
							pp[j] = i;
							j++;
							//printf("j = %d ntask = %d\n",j,ntask);
							if(ntask - 1 == j)
							{
								printf("\n*****Iniciando verifição de resultados*****\n");
								mcadast[pp[0]][4] = 0;
								mcadast[pp[1]][4] = 0;
								mcadast[pos][4] = 0;
								vec[2] = mcadast[pos][0];
								hash(vec);//iniciaVerifica();
								j = 0;
								break;					
					

							}
						}
					}

				
			}
		}		
	}
	

}
int main()
{
		int sockfd, portno, clilen,n,i,j;
		long  newsockfd;	
		struct sockaddr_in serv_addr, cli_addr;
		for(i = 0;i < tam;i++)
		{
			for(j = 0; j < 10;j++)
			{
				l[i].log[j] = 0;
			}
		}
		for(i = 0;i < tam;i++)
		{
			for(j = 0;j < niv;j++)
			{
				mcadast[i][j] = 0;
			}
		}
		for(i = 0; i < tam; i++)
		{
	
			TREC[i] = 0;
			TREA[i] = 0;
			TCOR[i] = 0;
			CAT[i] = 4;
		}
	    	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	    	if (sockfd < 0) 
	    	{
	    	    perror("ERRO ao abrir");
	    	    exit(1);
	    	}
	   
	    	bzero((char *) &serv_addr, sizeof(serv_addr));
	    	portno = 50000;
	    	serv_addr.sin_family = AF_INET;
	    	serv_addr.sin_addr.s_addr = INADDR_ANY;
	    	serv_addr.sin_port = htons(portno);
	 
    		if (bind(sockfd, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0)
    		{
    		     perror("ERRO no bind");
    		     exit(1);
    		}

 	  	listen(sockfd,5);
 	   	clilen = sizeof(cli_addr);
				while(1)
	        {	
				  			
			newsockfd = accept(sockfd, (struct sockaddr *)&cli_addr, &clilen);
		    	if (newsockfd < 0) 
		    	{
				perror("Erro para aceitar");
				exit(1);
		    	}	
			pthread_t thread;
			pthread_create(&thread, NULL, &func, (void *)newsockfd);	
			pthread_join(thread);	
		}		    	    
}
