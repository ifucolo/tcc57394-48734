//Sistema de classificação/consulta de categoria

int consulta(int trec, int trea, int tcor){

int categoria;
double dtrec = (double) trec;
double dtrea = (double) trea;
double dtcor = (double) tcor;

// categoria: diamante = 0, ouro = 1, prata = 2, bronze = 3, sem-categoria = 4;

// Diamante:
if((dtrec >= 100) && ((dtrec/dtrea) < 1.176470588) &&  ((dtrec/dtcor) <= 1.25))
	{
	printf("\n\nCategoria: Diamante");
	categoria = 0;
	return categoria;
	}
	
// Ouro:
else if((dtrec >= 50) && ((dtrec/dtrea) < 1.25) &&  ((dtrec/dtcor) <= 1.25))
	{
	printf("\n\nCategoria: Ouro");
	categoria = 1;
	return categoria;
	}

// Prata:
else if((dtrec >= 15) && ((dtrec/dtrea) < 1.333333333) &&  ((dtrec/dtcor) <= 1.666666667))
	{
	printf("\n\nCategoria: Prata");
	categoria = 2;
	return categoria;
	}
	
// Bronze:
else if((dtrec >= 6) && ((dtrec/dtrea) < 1,428571429) &&  ((dtrec/dtcor) <= 1.666666667))
	{
	printf("\n\nCategoria: Bronze");
	categoria = 3;
	return categoria;
	}
	
// Sem categoria:
else
	{
	printf("\n\nCategoria: Sem Categoria\n");
	categoria = 4;
	return categoria;
	}





}
